function myfilter(ele,cb){
    if(!ele || !cb){
        return;
    }
    let element = [];
    for (let i=0 ; i<ele.length ; i++){
        if(cb(ele[i]) == true){
            element.push(ele[i]);
        }
    }
    return element;
}
module.exports = myfilter;