let arr = []
function flatten(elements) {
    if (!elements){
        return [];
    };
    if (typeof elements === 'number' || typeof elements === 'string'){
        arr.push(elements)
    }
    else{    
        for (let i = 0; i < elements.length; i++) {
            flatten(elements[i])
        }
    }
    return arr
}

module.exports = {flatten}