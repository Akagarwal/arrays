function find(ele, cb) {
    if (!ele || !cb)
    {
        return undefined
    }
    for (let i = 0; i < ele.length; i++) {
        if (cb(ele[i]))
        {
            return ele[i]
        }
    }
    return undefined
}

const checkEven = (num) => {
    if (num % 2 == 0)
    {
        return true
    }
    else
    {
        return false
    }
}

module.exports = {find,checkEven};
