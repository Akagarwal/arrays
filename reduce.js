function reduce(elements,initialVal){
    let accumulator = initialVal === undefined ? 0 : initialVal
    for(let i=0; i<elements.length ; i++){
        accumulator=reduce(accumulator, elements[i],i,elements);
    }
    return accumulator;
};

module.exports = reduce;